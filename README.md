# ephdat2asc


Convert the inpop ephemeris file (..._littleendian.dat) to an ascii file compatible with asc2eph.

This program is only compatible with the named ephemeris "*Binary files compatible with the JPL file format*"
on the download pages of the ephemeris INPOP  https://www.imcce.fr/inpop.
These files have usually the suffix **littleendian.dat**

## Compilation

### Requirements

 - cmake 
 - fortran compiler, compliant with fortran 2003, such as gfortran or ifort.

### Commands

In a terminal window, execute the following commands to compile ephdat2asc :
```shell
git clone https://gitlab.obspm.fr/ephtools/ephdat2asc.git
cd ephdat2asc
mkdir build
cd build
cmake ..
cmake --build .
````

The executable is now available in the folder build :  **build/ephdat2asc**


### Execution

Example to extract the header and coefficients of the planetary solution INPOP19A  :

```shell
./ephdat2asc inpop19a_TDB_m100_p100_littleendian.dat extractinpop19a
```

This command produces two text files: 
 - extractinpop19a.header : header block of the ephemeris file (constants)
 - extractinpop19a.asc : coefficients of the chebychev polynomials
