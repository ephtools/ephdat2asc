!********************************************************
!>  \file opencmn.f90
!!
!!  \brief open the binary file and detect the record size and ksize
!!            
!! Author : M. GASTINEAU, CNRS, IMCCE, Observatoire de Paris, PSL
!!
!! Copyright, 2020, CNRS
!!
!<
!********************************************************
        subroutine opencmn(fnum, fich1, ksize)
         implicit none
         integer, intent(in) ::  fnum
         integer, intent(inout) :: ksize
         character(len=*), intent(in) :: fich1
         integer lrecl, ierr         
         logical fexist
         
         integer(4) denum
         character*6 ttl(14,3),cnam(400)
         real(8):: ss(3), au, emrat, ssrec3(2), ssrec4(2)
         integer(4) :: ipt(3,12),lpt(3), ncon
         integer(4) recordsize
         
         inquire (file=fich1,exist=fexist)
         if (.not.fexist) then
            write(*,*) 'File does not exist'
            write(*,*) "Aborting ephdat2asc on error !"
            stop
         else
            if (ksize.eq.0) then
             lrecl = 4*2036
            else
             lrecl=4*ksize
            endif
            open (fnum,file=fich1,access='direct',                             &
     &            recl=lrecl,status='old',iostat=ierr)
            if (ierr.ne.0) then
               write (*,'(/2x,''File error: '',i4/)') ierr
               write(*,*) "Aborting ephdat2asc on error !"
               close (11)
               stop
            endif
            
            if (ksize.eq.0) then
                read (fnum,rec=1) ttl,cnam,ss,ncon,au,emrat,ipt,denum,lpt, recordsize
                close(fnum)
                lrecl=8*recordsize
                open (fnum,file=fich1,access='direct',   recl=lrecl,status='old',iostat=ierr)
                ksize = recordsize*2
                write(*,*) '    verbose debug : KSIZE pour INPOP:', ksize
                read(fnum, rec=3) ssrec3
                read(fnum, rec=4) ssrec4
                if ((ssrec3(1).ne.ss(1)).or.(ssrec4(1).ne.ssrec3(2))) then 
                    write(*,*) "Times of the first coefficient record ", ssrec3
                    write(*,*) "Times of the second coefficient record ", ssrec4
                    write(*,*) "Aborting ephdat2asc on error to find the fortran record size !"
                    stop
                endif
                read (fnum,rec=1) ttl,cnam,ss,ncon,au,emrat,ipt,denum,lpt, recordsize
            endif
            
         endif
         return 
         end
