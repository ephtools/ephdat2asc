!********************************************************
!>  \file ephdat2asc.f90
!!
!!  \brief Dump a binary ephemery file (old JPL format) to ascii file (compliant with the JPL tools asceph)
!!  \details the binary ephemeis file shoudl have the endianess as the computer running this program
!!            
!! Author : M. GASTINEAU, CNRS, IMCCE, Observatoire de Paris, PSL
!!
!! Copyright, 2020, CNRS
!!
!<
!********************************************************
program ephdat2asc
      use getparam
      use version
      use header
      use coeff
      implicit none
      logical fexist
      integer(4) denum
      character(len=256) fich1,fich2, fichheader, fichcoeff
      integer ksize

    write (*,'(76(''=''))')
    write (*,*) "EPHDAT2ASC - ", strversion
    write (*,'(76(''=''))')
    
    call getparamfilename(fich1, fich2)
    fichheader = trim(fich2)//".header"
    call dumpheader(fich1, fichheader, ksize) 
    fichcoeff = trim(fich2)//".asc"
    call dumpcoeff(fich1, fichcoeff, ksize) 
    write (*,*)
    write (*,*) " Terminate the execution of ephdat2asc.... OK"
    stop
end