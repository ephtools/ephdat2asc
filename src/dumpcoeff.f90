!********************************************************
!>  \file dumpcoeff.f90
!!
!!  \brief dump the coefficients
!!            
!! Author : M. GASTINEAU, CNRS, IMCCE, Observatoire de Paris, PSL
!!
!! Copyright, 2020, CNRS
!!
!<
!********************************************************
!
module coeff
    contains
        subroutine dumpcoeff(fich1, fich2, ksize)
            use iso_fortran_env
            implicit none
            character(len=*), intent(in):: fich1,fich2
            integer, intent(in) :: ksize
            integer j, jp2, k, ncoeff, countblock
            integer  ierr
            real(8), dimension(:),allocatable:: coeffs
            character(len=256) iomessage
      
            write(*,*) 
            write(*,*) "Copy from ", trim(fich1), " to ", trim(fich2)
            write(*,*) 
    
            ncoeff = ksize/2
            countblock = 1
            allocate(coeffs(1:ncoeff))


            open  (12,file=fich2)
            write (*,*) " Convert the coefficients...."
            
            ierr = 0
            do while (ierr.eq.0)
                read(11, iostat=ierr,rec=countblock+2,iomsg=iomessage) (coeffs(j), j=1,ncoeff)
                if ((ierr.eq.36).or.(ierr.eq.5002)) then ! code : 36 for intel compilers, 5002 for gnu fortran
                    write (*,*) " ",trim(fich2), " is completely generated.... OK"
                    close(12)
                    return 
                endif
                if (ierr.ne.0) then
                    write (*,'(/2x,''File error: '',i4/)') ierr
                    write (*,*) iomessage
                    write (*,*) "Aborting ephdat2asc on error !"
                    close (11)
                    close(12)
                    stop 
                endif
                if ( mod(countblock,100).eq.0) then
                    write (*,*) 'writing the record', countblock
                endif  
                write (12,'(2I6)') countblock, ncoeff
                do j=1,ncoeff, 3
                 jp2 = MIN(j+2,ncoeff)
                 write (12,'(3(d24.16,1X))') (coeffs(k),k=j,jp2)
                enddo
                countblock = countblock+1
                  
            end do
            
            close(11)
            close(12)
            write (*,*)
            write (*,*) " ",trim(fich2), " is completely generated.... OK"
        end subroutine
end module