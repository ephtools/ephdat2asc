!********************************************************
!>  \file getparamfilename.f90
!!
!!  \brief return the parameter files on command line
!!            
!! Author : M. GASTINEAU, CNRS, IMCCE, Observatoire de Paris, PSL
!!
!! Copyright, 2020, CNRS
!!
!<
!********************************************************

module getparam
contains
    subroutine getparamfilename(filenamedat, filenameasc)
    implicit none
    character(len=*), intent(out) ::  filenamedat
    character(len=*), intent(out) ::  filenameasc
    integer num, len, status


    num = 1
    call GET_COMMAND_ARGUMENT(num, filenamedat, len, status)
    if ((status.ne.0)) then
        write(*,*) "Error : Invalid arguments."
        write(*,*) "Usage : ephdat2asc ephemsrc.dat ephemdst.asc"
        write(*,*) "        Copy the existing binary file ephemsrc.dat to the ascii file ephemdst.asc" 
        write(*,*) "Aborting ephdat2asc on error !"
        stop 
    endif
    num = 2
    call GET_COMMAND_ARGUMENT(num, filenameasc, len, status)
    if ((status.ne.0)) then
        write(*,*) "Error : Invalid arguments."
        write(*,*) "Usage : ephdat2asc ephemsrc.dat ephemdst.asc"
        write(*,*) "        Copy the existing binary file ephemsrc.dat to the ascii file ephemdst.asc" 
        write(*,*) "Aborting ephdat2asc on error !"
        stop
    endif        
        
    end subroutine
end module getparam       
