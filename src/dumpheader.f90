!********************************************************
!>  \file dumpheader.f90
!!
!!  \brief dump the header file
!!            
!! Author : M. GASTINEAU, CNRS, IMCCE, Observatoire de Paris, PSL
!!
!! Copyright, 2020, CNRS
!!
!<
!********************************************************
!
module header
contains
    subroutine dumpheader(fich1, fich2, ksize)
        implicit none
        character(len=*), intent(in):: fich1,fich2
        integer, intent(out) :: ksize
        logical fexist
        integer(4) denum
        character*6 ttl(14,3),cnam(400),namefirsline(10)
        real(8) :: ss(3),cval(400), au, emrat
        real(8) :: rdenum
        integer(4) :: ipt(3,12),lpt(3),rpt(3),tpt(3)
        integer(4) recordsize
        integer i, j
        integer len, ncon
        logical enablett 
        character*20 fmtval
        
        enablett = .true.
  
        write(*,*) 
        write(*,*) "Copy from ", trim(fich1), " to ", trim(fich2)
        write(*,*) 

        ksize = 0
        rpt(:) = 0
        tpt(:) = 0
  
        call opencmn(11, fich1, ksize) 
        read (11,rec=1) ttl,cnam,ss,ncon,au,emrat,ipt,denum,lpt
        read (11,rec=2) (cval(i),i=1,ncon)

        do i=1, ncon
            if (cnam(i).eq.'VERSIO') then
                rdenum=2000*10+cval(i)*10
            endif    
        end do
        write(*,*) 'version of the ephemerids encoded as', rdenum
  !
  !     -------------------------
  !     Print the constant
  !     -------------------------
  ! 
        open  (12,file=fich2)
        write (*,*) " Convert the header...."
        
        write (12,'(A6,I4,10X,A7,I4)') 'KSIZE=', ksize,'NCOEFF=', ksize/2
        write (12,*) ' '
        write (12,'(A)') "GROUP   1010"
        write (12,*) ' '
        write (12,*) ' Planetary Ephemeris', denum
        write (12,*) ' Start Epoch: JED=',ss(1) 
        write (12,*) ' Final Epoch: JED=',ss(2)
        write (12,*) ' '
        write (12,'(A)') 'GROUP   1030'
        write (12,*) ' '
        write (12,'(10x,3f23.3)') ss
        write (12,*) ' '
        write (12,'(A)') 'GROUP   1040'
        write (12,*) ' '
        !ncon=MIN(ncon, 400-3)
        write (12,'(I6)')   ncon+3
        
        namefirsline(1)='DENUM'
        namefirsline(2)='AU'
        namefirsline(3)='EMRAT'
        namefirsline(4:10) = cnam(1:7)
        write (12,'(10(A8))') (namefirsline(i), i=1, 10)
        do j=8, ncon, 10
         len = 10
         if (j+10>ncon) then
          len = ncon-j+1
         endif
         write(fmtval,'(A1,I1,A5)') '(',len,'(A8))'
         write (12,fmtval) (cnam(j+i), i=0, len-1)
        enddo
  
        write (12,*) ' '
        write (12,'(A)') 'GROUP   1041'
        write (12,*) ' '
        write (12,'(I6)')   ncon+3
        write (12,'(3(d25.17))') rdenum, au,emrat
        do j=1, ncon, 3
         len = 3
         if (j+3>ncon) then
          len = ncon-j+1
         endif
         write(fmtval,'(A1,I1,A9)') '(',len,'(d25.17))'
         write (12,fmtval) (cval(j+i), i=0, len-1)
        enddo
       
        write (12,*) ' '
        write (12,'(A)') 'GROUP   1050'
        write (12,*) ' '
        if (enablett.eqv..true.) then
            write (12,'(15(i6))') (ipt(1,j),j=1,12), lpt(1),rpt(1),tpt(1)
            write (12,'(15(i6))') (ipt(2,j),j=1,12), lpt(2),rpt(2),tpt(2)
            write (12,'(15(i6))') (ipt(3,j),j=1,12), lpt(3),rpt(3),tpt(3)
        else
            write (12,'(13(i6))') (ipt(1,j),j=1,12), lpt(1)
            write (12,'(13(i6))') (ipt(2,j),j=1,12), lpt(2)
            write (12,'(13(i6))') (ipt(3,j),j=1,12), lpt(3)
        endif
        write (12,*) ' '
        write (12,'(A)') 'GROUP   1070'
        close(12)
        write (*,*)
        write (*,*) " ",trim(fich2), " is completely generated.... OK"
    end subroutine
end module